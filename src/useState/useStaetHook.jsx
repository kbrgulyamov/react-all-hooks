import React, { useState } from "react";

const UseStaethook = (props) => {
      const [counter, setCounter] = useState(0)


      const incriment = () => {
        setCounter(counter + 1)
      }

      const decriment = () => {
            setCounter(counter - 1)
      }

      const res = () => {
            setCounter(0)
      }
      return (
            <div  className="container">
                  <h1>Counter: {counter}</h1>
                  <button onClick={incriment}> Incriment: ( + )</button>
                  <button onClick={decriment}> Decriment: ( - )</button>
                  <button onClick={res}> Return 0: ( 0 )</button>
                  <h1>useStaet</h1>
            </div>
      )
}

export default UseStaethook