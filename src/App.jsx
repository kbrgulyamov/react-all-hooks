import './App.css';
import UseStaethook from './useState/useStaetHook';
import UseEffecthook from './useEfect/useEfecthook';
import UseStateUseEff from './useStateUseEffect/useState.UseEff';
import Memo from './useMemo/usememo';
import InputValue from './onchaench/input';


function App() {
  return (
    <div className="App">
      <UseStaethook />
      <UseStateUseEff />
      <Memo />
      <InputValue />
      <UseEffecthook />
    </div>
  );
}

export default App;
