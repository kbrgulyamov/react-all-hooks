import React, { useState, useMemo } from "react";


const complexCompute = (num) => {
      let i = 0
      while (i < 1000) i++
      return num * 2
}


const Memo = () => {
      const [number, setNumber] = useState(45)
      const [colored, setColored] = useState(false)

      const computed = useMemo(() => {
            return complexCompute(number)
      }, [number])


      const styles = {
            color: colored ? "red" : "#fff"
      }

      return (
            <div className="container_4">
                  <div className="contaent_insaett">
                        <h1 className="title" style={styles}>Вычисляемое своество: {computed}</h1>
                        <button className="Add" onClick={() => setNumber(prev => prev + 1)}>Add</button>
                        <button className="delete" onClick={() => setNumber(prev => prev - 1)}>Delete</button>
                        <button onClick={() => setColored(prev => !prev)}>Change</button>
                  </div>
            </div>
      )
}


export default Memo