import React, {useState, useEffect, useRef} from "react";

// let renderCounder = 1

const UseStateUseEff = () => {
      const [value, setValue] = useState('')
      const renderCounder = useRef(0)
      const inputRef = useRef(null)
      const prevValue = useRef('')

      useEffect(() => {
            renderCounder.current++

      })

      useEffect(() => {
       prevValue.current = value
      }, [value])

      const focus = () => inputRef.current.focus()

      return (
            <div className="container_3">
                  <h1>Counter Rendiring: {renderCounder.current}</h1>
                  <h2>Prev Rendiring: {prevValue.current}</h2>
                  <input ref={inputRef} type="text"  onChange={e => setValue(e.target.value)} value={value}/>
                  <button  onClick={focus}>Fokus</button>
                  <h1>useState && useEffect && useRef</h1>
            </div>
      )
}

export default UseStateUseEff