import React, {useState, useEffect} from "react";

const UseEffecthook = () => {
      const [type, setType] = useState('users')
      const [data, setData] = useState([])

       useEffect(() => {
            fetch(`https://jsonplaceholder.typicode.com/${type}`)
            .then(response => response.json())
            .then(json => setData(json))
       }, [type])
      return (
            <div className="conatiner_2">
                  <h1>Name: {type}</h1>
                  <button onClick={() => setType('users')}>Пользователи</button>
                  <button onClick={() => setType('todos')}>Todos</button>
                  <button onClick={() => setType('posts')}>Posts</button>
                  <pre>{JSON.stringify(data, null, 2)}</pre>
                  <h1>useEffect</h1>
            </div>
      )
}

export default UseEffecthook