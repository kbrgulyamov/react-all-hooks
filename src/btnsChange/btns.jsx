import React, { useState } from 'react'

const Btns = () => {
      const [colored, setColored] = useState(false)

      const styles = {
            backgroundColor: colored ? "red" : "none"
      }
      return (
            <div className="btn_wrapper">
                  <button style={styles} onClick={() => setColored(prev => !prev)}>Click BG Changes</button>
                  <button>Click BG Changes</button>
                  <button>Click BG Changes</button>
                  <button>Click BG Changes</button>
            </div>
      )
}

export default Btns